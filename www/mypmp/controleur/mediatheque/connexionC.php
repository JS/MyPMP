<?php
	session_start();
	
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');

// connexion à la base de données
	$bdd = connexionPDO($config);

//hachage du mot de passe
	$_POST['mdp'] = sha1('gz' . $_POST['mdp']);
	
	$resultat = fn_connexion($bdd, $_POST);

	if (!$resultat){
//suppression des variables de session
		$_SESSION = array();
		session_destroy();
		include_once ('../../vue/mediatheque/connexion.php');
	}elseif(isset($resultat) AND $resultat['id_profil']==1){
		$_SESSION['id_utilisateur'] = $resultat['id_utilisateur'];
		$_SESSION['login'] = $resultat['login'];
		$_SESSION['profil'] = $resultat['id_profil'];
//récupération du timestamp de la connexion (pour mise en place d'un timeout)
		$_SESSION['horodatage'] = time();
//récupération de l'adresse ip du client (pour protection contre vol de sessions)
		$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
		header ('Location: ../../vue/mediatheque/admin.php');
	}