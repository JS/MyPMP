<?php
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');
	
// connexion à la base de données
	$bdd = connexionPDO($config);

// redondance des structures conditionnelles. A FACTORISER !!!
//recherche (simple ou avancée)
	if (isset($_POST['choix']) OR isset($_POST['rechercher'])){
		if (isset($_POST['rechercher'])){
			$donnees = fn_recherche($bdd, $_POST);
//incomplet. A TERMINER !
		}
		include_once ('../../vue/mediatheque/recherche.php');

//recherche en vue suppression ressource
	}elseif (isset($_POST['supprimer'])){
		if($_POST['type']=='tous'){
//recherche globale sur tous les types de ressources
			$donnees = fn_recherche($bdd, $_POST);
		}else{
//recherche restreinte à un type de ressource
			$donnees = fn_rechercheR($bdd, $_POST);
		};
		include_once ('../../vue/mediatheque/suppression.php');

//recherche en vue modification ressource
	}elseif (isset($_POST['modifier'])){
		if($_POST['type']=='tous'){
//recherche globale sur tous les types de ressources
			$donnees = fn_recherche($bdd, $_POST);
		}else{
//recherche restreinte à un type de ressource
			$donnees = fn_rechercheR($bdd, $_POST);
		};
		include_once ('../../vue/mediatheque/modification.php');

//recherche en vue prêt ressource
	}elseif (isset($_POST['preter'])){
		if($_POST['type']=='tous'){
//recherche globale sur tous les types de ressources
			$donnees = fn_recherche($bdd, $_POST);
		}else{
//recherche restreinte à un type de ressource
			$donnees = fn_rechercheR($bdd, $_POST);
		};
		include_once ('../../vue/mediatheque/pret.php');

//recherche en vue récupération ressource
	}elseif (isset($_POST['recuperer'])){
		if($_POST['type']=='tous'){
//recherche globale sur tous les types de ressources
			$donnees = fn_recherche($bdd, $_POST);
		}else{
//recherche restreinte à un type de ressource
			$donnees = fn_rechercheR($bdd, $_POST);
		};
		include_once ('../../vue/mediatheque/pret.php');
	}