<?php

	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');

// connexion à la base de données	
	$bdd = connexionPDO($config);

//récupération du type de ressources souhaité via GET
	if (isset($_GET['choix'])){
		$_POST['choix'] = $_GET['choix'];
	}

//choix du type de ressource à afficher
//pour consulter la totalité des ressources (tous les types)
	if (!(isset($_POST['choix'])) OR ($_POST['choix'] == 'tout')){
//affectation d'une valeur à $_POST['choix'] en vu de renseigner $_GET sur consultation.php
		$_POST['choix'] = 'tout';
//décompte du nombre total de ressources
		$nbEntrees = fn_comptage($bdd);
//numéro de la page courante récupéré par GET (par défaut = 1)
		if (empty($_GET['p'])){
			$p = 1;
		}else{
			$p = (int) $_GET['p'];
		}
//défition du nombre de ressources affichées par page
		$affichage = 8;
//détermination de la ressource à partir de laquelle débute l'affichage
		$demarrage = $p * $affichage - $affichage;
//calcul du nombre de pages (arrondi à l'entier supérieur)
		$nbPages = (int) ceil($nbEntrees['0'] / $affichage);
		$donnees = fn_consultationGlobale($bdd, $demarrage, $affichage);
	}else{
//pour consulter uniquement un type de ressource (livre, mag., cd, dvd, autre)
		if ($_POST['choix']=='les livres'){
			$_POST['choix']='livre';
		}elseif($_POST['choix']=='les magazines'){
			$_POST['choix']='magazine';
		}elseif($_POST['choix']=='les CD audio'){
			$_POST['choix']='CD audio';
		}elseif($_POST['choix']=='les DVD'){
			$_POST['choix']='DVD';
		}elseif($_POST['choix']=='autres'){
			$_POST['choix']='autre';
		}
//décompte du nombre de ressources
		$nbEntrees = fn_comptagePartiel($bdd, $_POST);
//numéro de la page courante récupéré par GET (par défaut = 1)
		if (empty($_GET['p'])){
			$p = 1;
		}else{
			$p = (int) $_GET['p'];
		}
//défition du nombre de ressources affichées par page
		$affichage = 8;
		$demarrage = $p * $affichage - $affichage;
//calcul du nombre de pages (arrondi à l'entier supérieur)
		$nbPages = (int) ceil($nbEntrees['0'] / $affichage);

		$donnees = fn_consultationRestreinte($bdd, $demarrage, $affichage, $_POST);
	}