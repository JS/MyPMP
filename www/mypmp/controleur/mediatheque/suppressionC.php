<?php
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');

// connexion à la base de données
	$bdd = connexionPDO($config);
	
	$donneesA = fn_affichageDocument($bdd, $_POST);
	
	fn_suppression($bdd, $_POST);
	
	include_once ('../../vue/mediatheque/suppression.php');