<?php
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');

// connexion à la base de données
	$bdd = connexionPDO($config);

	if (!empty($_POST['type']) AND !empty($_POST['titre'])){
		fn_ajout($bdd, $_POST);
	}
	
	include_once ('../../vue/mediatheque/ajout.php');