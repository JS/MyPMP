<?php
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');

// connexion à la base de données
	$bdd = connexionPDO($config);

	if($_POST['modification']=='modifier'){
		fn_modificationDoc($bdd, $_POST);
		fn_modificationAut($bdd, $_POST);
		fn_modificationLang($bdd, $_POST);
	}	
	
		$donneesA = fn_affichageDocument($bdd, $_POST);

		include_once ('../../vue/mediatheque/modification.php');