<?php
//fixation de la durée d'inactivité autorisée avant fermeture automatique de session (timeout)
	$session_timeout = 600;

//vérification de la présence des variables de session
	if (!isset($_SESSION['login']) OR !isset($_SESSION['profil']) OR !isset($_SESSION['horodatage']) OR !isset($_SESSION['ip'])){
		$_SESSION = array();
		session_destroy();
		header ('Location: ../../vue/mediatheque/connexion.php');
		exit();
	}

//vérification du profil de l'utilisateur
	if ($_SESSION['profil']!=1){
		$_SESSION = array();
		session_destroy();
		header ('Location: ../../vue/mediatheque/connexion.php');
		exit();
	}

//vérification de l'ancienneté de la session (fermeture si expiration du timeout)
	if (time()-$_SESSION['horodatage'] > $session_timeout){
		$_SESSION = array();
		session_destroy();
		header ('Location: ../../vue/mediatheque/connexion.php');
		exit();
	}

//vérification de la concordance des adresses ip du client
	if ($_SERVER['REMOTE_ADDR'] != $_SESSION['ip']){
		$_SESSION = array();
		session_destroy();
		header ('Location: ../../vue/mediatheque/connexion.php');
		exit();
	}
//mise à jour du timestamp
	$_SESSION['horodatage'] = time();