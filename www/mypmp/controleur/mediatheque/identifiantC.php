<?php
	session_start();
	
// vérification de la validité de l'identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');
	
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');
	
// connexion à la base de données	
	$bdd = connexionPDO($config);
	
	//hachage du mot de passe
	$_POST['mdp'] = sha1('gz' . $_POST['mdp']);
	
	$resultat = fn_identifiant($bdd, $_SESSION['id_utilisateur'], $_POST);

	//suppression des variables de session
	$_SESSION = array();
	session_destroy();
	header ('Location: ../../vue/mediatheque/connexion.php');