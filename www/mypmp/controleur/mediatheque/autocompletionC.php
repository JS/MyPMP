<?php
	require_once ('../../modele/connexion_sql.php');
	require_once ('../../modele/mediatheque/fonctions.php');
	
	$bdd = connexionPDO($config);
	
// redondance des fonctions liées à l'autocomplétion. A FACTORISER !!!	
	$donneesA = fn_autocompletionA($bdd);
	$donneesL = fn_autocompletionL($bdd);
	$donneesP = fn_autocompletionP($bdd);
	$donneesN = fn_autocompletionN($bdd);
	$donneesE = fn_autocompletionE($bdd);