<?php
	require ('../../modele/connexion_sql.php');
	require ('../../modele/mediatheque/fonctions.php');

// connexion à la base de données
	$bdd = connexionPDO($config);
	
	$donneesA =	fn_affichageDocument($bdd, $_POST);

//filtre de vérification sur champ "email"
	$verifEmail = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
	if ($verifEmail !== false){
		fn_pret($bdd, $_POST);
	}
	include_once ('../../vue/mediatheque/pret.php');