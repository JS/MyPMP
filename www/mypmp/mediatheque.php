<?php

	require ('modele/connexion_sql.php');
	require ('modele/mediatheque/fonctions.php');
		
// connexion à la base de données
	$bdd = connexionPDO($config);
// nombre de ressources en base
	$nbRessources = fn_comptage($bdd);
// dernières ressources saisies
	$ressources = fn_consultationAccueil($bdd);
		
	include_once ('vue/mediatheque/accueil.php');