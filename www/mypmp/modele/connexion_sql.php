<?php
// paramètres connexion
$config = array(
	'host'=>'localhost',
	'username'=>'root',
	'password'=>'',
	'database'=>'mediatheque',
	'charset' =>'utf8'
	);
	
// fonction de connexion à la base de données
function connexionPDO($config){
	$dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['database'] . ';charset=' . $config['charset'];
//DSN = Data Source Name
	return new PDO($dsn, $config['username'], $config['password']);
}