<?php

// FONCTIONS DE DENOMBREMENT :
// détermination du nombre de ressources stockées dans la base de données
function fn_comptage(PDO $bdd){
	$requete = 'SELECT COUNT(documents.id_document) FROM documents';
	$reponse = $bdd->query($requete);
	$result = $reponse->fetch();
	$reponse->closeCursor();
	return $result;
}

// détermination du nombre de ressources d'un type donnée stockées dans la base
function fn_comptagePartiel(PDO $bdd, $array){
	$requete = 'SELECT COUNT(documents.id_document)
		FROM documents
		INNER JOIN types ON types.id_type = documents.id_type
		WHERE type = :type';
	$reponse = $bdd->prepare($requete);
	$reponse->bindValue('type', $_POST['choix'], PDO::PARAM_STR);
	$reponse->execute();
	$result = $reponse->fetch();
	$reponse->closeCursor();
	return $result;
}


// FONCTIONS D'AFFICHAGE :
// détermination des 3 dernières ressources saisies dans la base pour affichage sur la page d'accueil
function fn_consultationAccueil(PDO $bdd){
	$requete ='SELECT documents.id_document, documents.titre, documents.annee_edition, documents.commentaire, types.type, auteurs.auteur, langues.langue
				FROM documents
				INNER JOIN types ON types.id_type = documents.id_type
				LEFT JOIN creer ON documents.id_document = creer.id_document
				LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
				LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
				LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
				ORDER BY documents.id_document DESC
				LIMIT 0, 3';
	$reponse = $bdd->query($requete);
	$result = $reponse->fetchAll();
	$reponse->closeCursor();
	return $result;
}

// détermination des documents à afficher (en tenant compte de la pagination) lors d'une consultation de toutes les ressources
function fn_consultationGlobale(PDO $bdd, $demarrage, $affichage){
	$requete ='SELECT documents.titre, documents.annee_edition, documents.commentaire, documents.id_type, types.type, auteurs.auteur, langues.langue
				FROM documents
				INNER JOIN types ON types.id_type = documents.id_type
				LEFT JOIN creer ON documents.id_document = creer.id_document
				LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
				LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
				LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
				ORDER BY id_type, auteur, titre
				LIMIT :start, :offset';
	$reponse = $bdd->prepare($requete);
	$reponse->bindValue('start', $demarrage, PDO::PARAM_INT);
	$reponse->bindValue('offset', $affichage, PDO::PARAM_INT);
	$reponse->execute();
	$result = $reponse->fetchAll();
	$reponse->closeCursor();
	return $result;
}

// détermination des documents à afficher (en tenant compte de la pagination) lors d'une consultation ne concernant qu'un type de ressources
function fn_consultationRestreinte(PDO $bdd, $demarrage, $affichage, $array){
	$requete ='SELECT documents.titre, documents.annee_edition, documents.commentaire, types.type, auteurs.auteur, langues.langue
				FROM documents
				INNER JOIN types ON types.id_type = documents.id_type
				LEFT JOIN creer ON documents.id_document = creer.id_document
				LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
				LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
				LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
				WHERE type = :type
				ORDER BY auteur, titre
				LIMIT :start, :offset';
	$reponse = $bdd->prepare($requete);
	$reponse->bindValue('type', $_POST['choix'], PDO::PARAM_STR);
	$reponse->bindValue('start', $demarrage, PDO::PARAM_INT);
	$reponse->bindValue('offset', $affichage, PDO::PARAM_INT);
	$reponse->execute();
	$result = $reponse->fetchAll();
	$reponse->closeCursor();
	return $result;
}

// récupération des informations du document (à supprimer/modifier/prêter) pour affichage	
function fn_affichageDocument(PDO $bdd, $array){
	$id = intval ($_POST['identifiant']);
	$requete ='SELECT documents.id_document, documents.titre, documents.annee_edition, documents.commentaire, types.type, auteurs.id_auteur, auteurs.auteur, langues.id_langue, langues.langue
			FROM documents
			INNER JOIN types ON types.id_type = documents.id_type
			LEFT JOIN creer ON documents.id_document = creer.id_document
			LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
			LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
			LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
			WHERE documents.id_document = :id';
	$reponse = $bdd->prepare($requete);
	$reponse->execute(array('id' => $id));
	$donneesA = $reponse->fetchAll();
	$reponse->closeCursor();
	return $donneesA;
}


// FONCTIONS D'AJOUT :
// ajout d'une ressource dans la base
function fn_ajout(PDO $bdd, $array){
// -> ajout dans table documents
	$requeteD = 'INSERT INTO documents (titre, annee_edition, commentaire, id_type) VALUES (:titre, :annee_edition, :commentaire, :id_type)';
	$ajoutD = $bdd->prepare($requeteD);
	$ajoutD->execute(array(
		'titre' => $_POST['titre'],
		'annee_edition' => (int) $_POST['annee'],
		'commentaire' => $_POST['commentaire'],
		'id_type' => $_POST['type']));
// récupération de l'identifiant "document" de la dernière ressource ajoutée
	$lastIdDocument = $bdd->lastInsertId();
	
// -> ajout dans table auteurs
	if ($_POST['auteur'] != NULL){
		$nomAuteur = $_POST['auteur'];
//utilisation de GROUP BY (en complément de COUNT) nécessaire au bon fonctionnement de la requête !!!
		$req = 'SELECT COUNT(*), auteurs.id_auteur FROM auteurs WHERE auteur = ? GROUP BY auteurs.id_auteur';
		$reqA = $bdd->prepare($req);
		$reqA->execute(array($nomAuteur));
		$row = $reqA->fetchAll();
		$reqA->closeCursor();

		if (!empty($row) AND $row[0][0] == 1){
//doublon détecté
			$lastIdAuteur = $row[0]['id_auteur'];
		}else{
//ajout du nouvel auteur dans la BDD
			$requeteA = 'INSERT INTO auteurs (auteur) VALUES (:auteur)';
			$ajoutA = $bdd->prepare($requeteA);
			$ajoutA->execute(array('auteur' => $_POST['auteur']));
// récupération de l'identifiant "auteur" de la dernière ressource ajoutée
			$lastIdAuteur = $bdd->lastInsertId();
		}
	}

// -> ajout dans table creer
	if ($_POST['auteur'] != NULL){
		$requeteC = 'INSERT INTO creer (id_document, id_auteur) VALUES (:id_document, :id_auteur)';
		$ajoutC = $bdd->prepare($requeteC);
		$ajoutC->execute(array(
			'id_document' => $lastIdDocument,
			'id_auteur' => $lastIdAuteur));
	}

// -> ajout dans table langues
	if ($_POST['langue'] != NULL){
		$nomLangue = $_POST['langue'];
//utilisation de GROUP BY (en complément de COUNT) nécessaire au bon fonctionnement de la requête !!!
		$reqL = 'SELECT COUNT(*), langues.id_langue FROM langues WHERE langue = ? GROUP BY langues.id_langue';
		$reqLangue = $bdd->prepare($reqL);
		$reqLangue->execute(array($nomLangue));
		$rowL = $reqLangue->fetchAll();
		$reqLangue->closeCursor();

		if (!empty($rowL) AND $rowL[0][0] == 1){
//doublon détecté
			$lastIdLangue = $rowL[0]['id_langue'];
		}else{
//ajout de la nouvelle langue dans la BDD
			$requeteL = 'INSERT INTO langues (langue) VALUES (:langue)';
			$ajoutL = $bdd->prepare($requeteL);
			$ajoutL->execute(array('langue' => $_POST['langue']));
// récupération de l'identifiant "langue" de la dernière ressource ajoutée
		$lastIdLangue = $bdd->lastInsertId();
		}
	}

// -> ajout dans table utiliser
	if ($_POST['langue'] != NULL){
		$requeteU = 'INSERT INTO utiliser (id_document, id_langue) VALUES (:id_document, :id_langue)';
		$ajoutU = $bdd->prepare($requeteU);
		$ajoutU->execute(array(
			'id_document' => $lastIdDocument,
			'id_langue' => $lastIdLangue));
	}
}


// FONCTIONS DE RECHERCHE :
// recherche indépendante du type de ressource sur les champs "titre" et "auteur"	
function fn_recherche(PDO $bdd, $array){
	$recherche = $_POST['recherche'];
	$requeteR ='SELECT documents.id_document, documents.titre, documents.annee_edition, documents.commentaire, types.type, auteurs.auteur, langues.langue, emprunter.date_emprunt, utilisateurs.nom, utilisateurs.prenom
	FROM documents
	INNER JOIN types ON types.id_type = documents.id_type
	LEFT JOIN creer ON documents.id_document = creer.id_document
	LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
	LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
	LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
	LEFT JOIN emprunter ON documents.id_document = emprunter.id_document
	LEFT JOIN utilisateurs ON emprunter.id_utilisateur = utilisateurs.id_utilisateur
	WHERE CONCAT_WS(" ", COALESCE(documents.titre, " "), COALESCE(auteurs.auteur, " ")) LIKE ?';
	$reponseR = $bdd->prepare($requeteR);
	$reponseR->execute(array('%' . $recherche . '%'));
	$donnees = $reponseR ->fetchAll();
	$reponseR->closeCursor();
	return $donnees;
}

// recherche restreinte (selon type de ressource)
function fn_rechercheR(PDO $bdd, $array){
	$recherche = $_POST['recherche'];
	$requeteR ='SELECT documents.id_document, documents.titre, documents.annee_edition, documents.commentaire, types.type, auteurs.auteur, langues.langue, emprunter.date_emprunt, utilisateurs.nom, utilisateurs.prenom
	FROM documents
	INNER JOIN types ON types.id_type = documents.id_type
	LEFT JOIN creer ON documents.id_document = creer.id_document
	LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
	LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
	LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
	LEFT JOIN emprunter ON documents.id_document = emprunter.id_document
	LEFT JOIN utilisateurs ON emprunter.id_utilisateur = utilisateurs.id_utilisateur
	WHERE type = ? 
	AND CONCAT_WS(" ", COALESCE(documents.titre, " "), COALESCE(auteurs.auteur, " ")) LIKE ?';
	$reponseR = $bdd->prepare($requeteR);
	$reponseR->execute(array($_POST['type'], '%' . $recherche . '%'));
	$donnees = $reponseR->fetchAll();
	$reponseR->closeCursor();
	return $donnees;
}


// FONCTION DE SUPPRESSION :
// suppression d'une ressource dans la base de données	
function fn_suppression(PDO $bdd, $array){
	$suppression = intval ($_POST['identifiant']);
	$requete = 'DELETE FROM documents WHERE id_document= :id';
	$reponse = $bdd->prepare($requete);
	$reponse->execute(array('id' => $suppression));
}


// FONCTIONS DE MODIFICATION :
// modification de certaines informations relatives à une ressource (--> titre, année, commentaire)
function fn_modificationDoc(PDO $bdd, $array){
	$modification = intval ($_POST['identifiant']);
	$requeteModif = 'UPDATE documents
		SET titre = :titre, 
		annee_edition = :annee, 
		commentaire = :commentaire 
		WHERE id_document= :id';
	$reponseModif = $bdd->prepare($requeteModif);
	$reponseModif->execute(array(
		'titre' => $_POST['titre'],
		'annee' => $_POST['annee'],
		'commentaire' => $_POST['commentaire'],
		'id' => $modification));
}

// modification de certaines informations relatives à une ressource (--> auteur)
function fn_modificationAut(PDO $bdd, $array){
	$id = intval ($_POST['identifiant']);
//récupération des données concernant la ressource à modifier
	$requeteA ='SELECT documents.id_document, auteurs.id_auteur, auteurs.auteur
			FROM documents
			INNER JOIN types ON types.id_type = documents.id_type
			LEFT JOIN creer ON documents.id_document = creer.id_document
			LEFT JOIN auteurs ON creer.id_auteur = auteurs.id_auteur
			WHERE documents.id_document = :id';
	$reponseA = $bdd->prepare($requeteA);
	$reponseA->execute(array('id' => $id));
	$donneesA = $reponseA->fetchAll();
	$reponseA->closeCursor();
	
	$idAuteur = $donneesA[0]['id_auteur'];
	$ancienAuteur = $donneesA[0]['auteur'];
	$nouvelAuteur = $_POST['auteur'];

	if ($ancienAuteur != $nouvelAuteur){
//on détermine combien de ressources possèdent le même identifiant auteur :
		$req = 'SELECT COUNT(*), creer.id_document FROM creer WHERE id_auteur = ? GROUP BY creer.id_document';
		$reqA = $bdd->prepare($req);
		$reqA->execute(array($idAuteur));
		$row = $reqA->fetchAll();
		$reqA->closeCursor();

		$nbreElements = count($row);

//1 seule ressource pour l'identifiant auteur donné :
		if($nbreElements == 1){
//on détermine si il existe un identifiant auteur correspondant au nouveau nom auteur :
			$req = 'SELECT auteurs.id_auteur FROM auteurs WHERE auteur = ? ';
			$reqB = $bdd->prepare($req);
			$reqB->execute(array($nouvelAuteur));
			$rowB = $reqB->fetch();
			$reqB->closeCursor();

//il existe un identifiant auteur correspondant au nouveau nom auteur :
			if (!empty($rowB)){
//on supprime dans la table creer la ligne associant l'identifiant de la ressource à l'identifiant de l'auteur à modifier :
				$requeteS = 'DELETE FROM creer WHERE id_auteur = :id';
				$reponseS = $bdd->prepare($requeteS);
				$reponseS->execute(array('id' => $idAuteur));
//on récupère l'identifiant de l'auteur existant :
				$idAuteur = $rowB['id_auteur'];
//on associe dans la table creer les identifiants ressource et auteur :
				if ($nouvelAuteur != NULL){
					$requeteC = 'INSERT INTO creer (id_document, id_auteur) VALUES (:id_document, :id_auteur)';
					$ajoutC = $bdd->prepare($requeteC);
					$ajoutC->execute(array('id_document' => $id, 'id_auteur' => $idAuteur));
				}
//il n'existe pas d'identifiant auteur correspondant au nouveau nom auteur :
			}else{
//on modifie le nom de l'auteur en conservant son ancien identifiant :
				$requeteModif = 'UPDATE auteurs SET auteur = :auteur WHERE id_auteur = :id';
				$reponseModif = $bdd->prepare($requeteModif);
				$reponseModif->execute(array('auteur' => $nouvelAuteur, 'id' => $idAuteur));
			}

//plusieurs ressources pour un même identifiant auteur :
		}else{
//on détermine si il existe un identifiant auteur correspondant au nouveau nom auteur :
			$req = 'SELECT auteurs.id_auteur FROM auteurs WHERE auteur = ? ';
			$reqB = $bdd->prepare($req);
			$reqB->execute(array($nouvelAuteur));
			$rowB = $reqB->fetch();
			$reqB->closeCursor();

//il existe un identifiant auteur correspondant au nouveau nom auteur :
			if (!empty($rowB)){
//on supprime dans la table creer la ligne associant l'identifiant de la ressource à l'identifiant de l'auteur à modifier :
				$requeteS = 'DELETE FROM creer WHERE id_document = :id';
				$reponseS = $bdd->prepare($requeteS);
				$reponseS->execute(array('id' => $id));
//on récupère l'identifiant de l'auteur existant :
				$idAuteur = $rowB['id_auteur'];
//on associe dans la table creer les identifiants ressource et auteur :
				if ($nouvelAuteur != NULL){
					$requeteC = 'INSERT INTO creer (id_document, id_auteur) VALUES (:id_document, :id_auteur)';
					$ajoutC = $bdd->prepare($requeteC);
					$ajoutC->execute(array('id_document' => $id, 'id_auteur' => $idAuteur));
				}
//il n'existe pas d'identifiant auteur correspondant au nouveau nom auteur :
			}else{
//on supprime dans la table creer la ligne associant l'identifiant de la ressource à l'identifiant de l'auteur à modifier :
				$requeteS = 'DELETE FROM creer WHERE id_document = :id';
				$reponseS = $bdd->prepare($requeteS);
				$reponseS->execute(array('id' => $id));
//on ajoute le nouvel auteur dans la table auteurs :
				$requeteA = 'INSERT INTO auteurs (auteur) VALUES (:auteur)';
				$ajoutA = $bdd->prepare($requeteA);
				$ajoutA->execute(array('auteur' => $nouvelAuteur));
//on récupère l'identifiant attribué au nouvel auteur :
				$lastIdAuteur = $bdd->lastInsertId();
//on associe dans la table creer les identifiants ressource et auteur :
				if ($nouvelAuteur != NULL){
					$requeteC = 'INSERT INTO creer (id_document, id_auteur) VALUES (:id_document, :id_auteur)';
					$ajoutC = $bdd->prepare($requeteC);
					$ajoutC->execute(array(
						'id_document' => $id,
						'id_auteur' => $lastIdAuteur));
				}
			}
		}
	}
}
	
// modification de certaines informations relatives à une ressource (--> langue)
function fn_modificationLang(PDO $bdd, $array){
	$id = intval ($_POST['identifiant']);
//récupération des données concernant la ressource à modifier
	$requete ='SELECT documents.id_document, langues.id_langue, langues.langue
			FROM documents
			INNER JOIN types ON types.id_type = documents.id_type
			LEFT JOIN utiliser ON documents.id_document = utiliser.id_document
			LEFT JOIN langues ON utiliser.id_langue = langues.id_langue
			WHERE documents.id_document = :id';
	$reponse = $bdd->prepare($requete);
	$reponse->execute(array('id' => $id));
	$donnees = $reponse->fetchAll();
	$reponse->closeCursor();
	
	$idLangue = $donnees[0]['id_langue'];
	$ancienneLangue = $donnees[0]['langue'];
	$nouvelleLangue = $_POST['langue'];

	if ($ancienneLangue != $nouvelleLangue){
//on détermine combien de ressources possèdent le même identifiant langue :
		$reqA = 'SELECT COUNT(*), utiliser.id_document FROM utiliser WHERE id_langue = ? GROUP BY utiliser.id_document';
		$repA = $bdd->prepare($reqA);
		$repA->execute(array($idLangue));
		$rowA = $repA->fetchAll();
		$repA->closeCursor();
		
		$nbreElements = count($rowA);

//1 seule ressource pour l'identifiant langue donné :
		if ($nbreElements == 1){
//on détermine si il existe un identifiant langue correspondant à la nouvelle langue :
			$reqB = 'SELECT langues.id_langue FROM langues WHERE langue = ? ';
			$repB = $bdd->prepare($reqB);
			$repB->execute(array($nouvelleLangue));
			$rowB = $repB->fetch();
			$repB->closeCursor();

//il existe un identifiant langue correspondant à la nouvelle langue :
			if (!empty($rowB)){
//on supprime dans la table utiliser la ligne associant l'identifiant de la ressource à l'identifiant de la langue à modifier :
				$reqC = 'DELETE FROM utiliser WHERE id_langue = :id';
				$repC = $bdd->prepare($reqC);
				$repC->execute(array('id' => $idLangue));
//on récupère l'identifiant de la langue existante :
				$idLangue = $rowB['id_langue'];
//on associe dans la table utiliser les identifiants ressource et langue :
				if ($nouvelleLangue != NULL){
					$reqD = 'INSERT INTO utiliser (id_document, id_langue) VALUES (:id_document, :id_langue)';
					$repD = $bdd->prepare($reqD);
					$repD->execute(array(
						'id_document' => $id,
						'id_langue' => $idLangue));
				}
//il n'existe pas d'identifiant langue correspondant à la nouvelle langue :
			}else{
//on modifie le nom de la langue en conservant son ancien identifiant :
				$reqE = 'UPDATE langues SET langue = :langue WHERE id_langue = :id';
				$repE = $bdd->prepare($reqE);
				$repE->execute(array(
					'langue' => $nouvelleLangue,
					'id' => $idLangue));
			}

//plusieurs ressources pour un même identifiant langue :
		}else{
//on détermine si il existe un identifiant langue correspondant à la nouvelle langue :
			$reqB = 'SELECT langues.id_langue FROM langues WHERE langue = ? ';
			$repB = $bdd->prepare($reqB);
			$repB->execute(array($nouvelleLangue));
			$rowB = $repB->fetch();
			$repB->closeCursor();

//il existe un identifiant langue correspondant à la nouvelle langue :
			if (!empty($rowB)){
//on supprime dans la table utiliser la ligne associant l'identifiant de la ressource à l'identifiant de la langue à modifier :
				$reqC = 'DELETE FROM utiliser WHERE id_document = :id';
				$repC = $bdd->prepare($reqC);
				$repC->execute(array('id' => $id));
//on récupère l'identifiant de la langue existante :
				$idLangue = $rowB['id_langue'];
//on associe dans la table utiliser les identifiants ressource et langue :
				if ($nouvelleLangue != NULL){
					$reqD = 'INSERT INTO utiliser (id_document, id_langue) VALUES (:id_document, :id_langue)';
					$repD = $bdd->prepare($reqD);
					$repD->execute(array(
						'id_document' => $id,
						'id_langue' => $idLangue));
				}
//il n'existe pas d'identifiant langue correspondant à la nouvelle langue :
			}else{
//on supprime dans la table utiliser la ligne associant l'identifiant de la ressource à l'identifiant de la langue à modifier :
				$reqE = 'DELETE FROM utiliser WHERE id_document = :id';
				$repE = $bdd->prepare($reqE);
				$repE->execute(array('id' => $id));
//on ajoute la nouvelle langue dans la table langues :
				$reqF = 'INSERT INTO langues (langue) VALUES (:langue)';
				$repF = $bdd->prepare($reqF);
				$repF->execute(array('langue' => $nouvelleLangue));
//on récupère l'identifiant attribué à la nouvelle langue :
				$lastIdLangue = $bdd->lastInsertId();
//on associe dans la table utiliser les identifiants ressource et langue :
				if ($nouvelleLangue != NULL){
					$reqG = 'INSERT INTO utiliser (id_document, id_langue) VALUES (:id_document, :id_langue)';
					$repG = $bdd->prepare($reqG);
					$repG->execute(array(
						'id_document' => $id,
						'id_langue' => $lastIdLangue));
				}
			}
		}
	}
}

// modification des identifiants de l'utilisateur
function fn_identifiant(PDO $bdd, $id, $array){
	$requete = 'UPDATE utilisateurs SET login = :login, mot_de_passe = :mdp WHERE id_utilisateur = :id';
	$reponse = $bdd->prepare($requete);
	$reponse->execute (array(
		'login' => $_POST['login'],
		'mdp' => $_POST['mdp'],
		'id' => $id));
}


// FONCTIONS DE PRET/RECUPERATION :
// prêt d'une ressource à un utilisateur
function fn_pret(PDO $bdd, $array){
//ajout dans table utilisateurs
	$nomEmprunteur = $_POST['nom'];
	$prenomEmprunteur = $_POST['prenom'];
	$emailEmprunteur = $_POST['email'];
//utilisation de GROUP BY (en complément de COUNT) nécessaire au bon fonctionnement de la requête !!!
	$req = 'SELECT COUNT(*), utilisateurs.id_utilisateur FROM utilisateurs WHERE nom = ? AND prenom = ? AND email = ? GROUP BY utilisateurs.id_utilisateur';
	$reqVerif = $bdd->prepare($req);
	$reqVerif->execute(array($nomEmprunteur, $prenomEmprunteur, $emailEmprunteur));
	$row = $reqVerif->fetchAll();
	$reqVerif->closeCursor();

	if ($row[0][0] >= 1){
		$lastIdUtilisateur = $row[0]['id_utilisateur'];
	}else{
		$requeteUt = 'INSERT INTO utilisateurs (nom, prenom, email) VALUES (:nom, :prenom, :email)';
		$ajoutUt = $bdd->prepare($requeteUt);
		$ajoutUt->execute(array(
			'nom' => $_POST['nom'],
			'prenom' => $_POST['prenom'],
			'email' => $_POST['email']));
// récupération de l'identifiant "utilisateur" du dernier emprunteur ajouté :
		$lastIdUtilisateur = $bdd->lastInsertId();
	}
	//ajout dans table emprunter
	$requeteEm = 'INSERT INTO emprunter (id_document, id_utilisateur, date_emprunt) VALUES (:id_document, :id_utilisateur, CURDATE())';
	$ajoutEm = $bdd->prepare($requeteEm);
	$ajoutEm->execute(array(
		'id_document' => $_POST['identifiant'],
		'id_utilisateur' => $lastIdUtilisateur));
}

function fn_recuperation(PDO $bdd, $array){
	$recuperation = intval ($_POST['identifiant']);
	$requeteRecup = 'DELETE FROM emprunter WHERE id_document= :id';
	$reponseRecup = $bdd->prepare($requeteRecup);
	$reponseRecup->execute(array(
		'id' => $recuperation));
}


// FONCTION DE CONNEXION :
// vérification de la conformité login + mot de passe lors de la tentative de connexion
function fn_connexion(PDO $bdd, $array){
	$requete = 'SELECT id_utilisateur, login, id_profil FROM utilisateurs WHERE login = :login AND mot_de_passe = :mdp';
	$verif = $bdd->prepare($requete);
	$verif->execute (array(
		'login' => $_POST['login'],
		'mdp' => $_POST['mdp']));
	$resultat = $verif->fetch();
	$verif->closeCursor();
	return $resultat;
}


// FONCTIONS D'AUTOCOMPLETION :
// redondance des fonctions liées à l'autocomplétion. A FACTORISER !!!
// récupération des auteurs (pour autocomplétion dans champ de saisie)
function fn_autocompletionA(PDO $bdd){
	$requeteA = 'SELECT auteur FROM auteurs';
	$reponse = $bdd->query($requeteA);
	$donneesA = $reponse->fetchAll();
	$reponse->closeCursor();
	return $donneesA;
}

// récupération des langues utilisées (pour autocomplétion dans champ de saisie)
function fn_autocompletionL(PDO $bdd){
	$requeteL = 'SELECT langue FROM langues';
	$reponse = $bdd->query($requeteL);
	$donneesL = $reponse->fetchAll();
	$reponse->closeCursor();
	return $donneesL;
}

// récupération des prénoms des utilisateurs (pour autocomplétion dans champ de saisie)
function fn_autocompletionP(PDO $bdd){
	$requeteP = 'SELECT prenom FROM utilisateurs';
	$reponse = $bdd->query($requeteP);
	$donneesP = $reponse->fetchAll();
	$reponse->closeCursor();
	return $donneesP;
}

// récupération des noms des utilisateurs (pour autocomplétion dans champ de saisie)
function fn_autocompletionN(PDO $bdd){
	$requeteN = 'SELECT nom FROM utilisateurs';
	$reponse = $bdd->query($requeteN);
	$donneesN = $reponse->fetchAll();
	$reponse->closeCursor();
	return $donneesN;
}

// récupération des emails des utilisateurs (pour autocomplétion dans champ de saisie)
function fn_autocompletionE(PDO $bdd){
	$requeteE = 'SELECT email FROM utilisateurs';
	$reponse = $bdd->query($requeteE);
	$donneesE = $reponse->fetchAll();
	$reponse->closeCursor();
	return $donneesE;
}