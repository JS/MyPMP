<?php
	include_once ('entete.php');

//vérif si identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');
?>

<h2>Gestion des prêts</h2>

<div>
	<form id="formPret1" action="../../vue/mediatheque/pret.php" method="post">
		<fieldset>
			<legend>Gestion des prêts</legend>
			<div>
				<label for="choix">Vous souhaitez : </label>
				<select name="choix" id="choix">
					<option value="p">prêter une ressource</option>
					<option value="r">récupérer une ressource</option>
				</select>
			</div>
			<div>
				<input type="submit" class="boutonV" name="choisir" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
	if (isset ($_POST['choix']) AND $_POST['choix'] == 'p'){
		echo '<h3>Quelle ressource souhaitez-vous prêter ?</h3>';
		include_once ('../../vue/mediatheque/form_recherche.php');
		echo '<input type="submit" class="boutonV" name="preter" value="valider" /></div></fieldset></form></div>';
	}elseif (isset ($_POST['choix']) AND $_POST['choix'] == 'r'){
		echo '<h3>Quelle ressource souhaitez-vous récupérer ?</h3>';
		$choix = $_POST['choix'];
		include_once ('../../vue/mediatheque/form_recherche.php');
		echo '<input type="submit" class="boutonV" name="recuperer" value="valider" /></div></fieldset></form></div>';
	}
?>

<?php
//PRET D'UNE RESSOURCE
	if (isset($_POST['preter']) AND isset($donnees[0])){
//récupération des valeurs de la colonne "date_emprunt" (fonction array_column)
		$emprunt = array_column($donnees, 'date_emprunt');
/*recherche de l'existence d'une valeur NULL dans le tableau contenant les valeurs de la colonne "date_emprunt" (fonction in_array)
Dans l'affirmative, affichage des ressources disponibles (non prêtées) répondant à la requête */
		if (in_array(NULL, $emprunt)){
			echo'<table>
			<tr>
			<th>id</th>
			<th>type</th>
			<th>titre</th>
			<th>auteur</th>
			<th>édition</th>
			<th>langue</th>
			<th>commentaire</th>
			</tr>';
			
			foreach($donnees as $donnee){
//affichage des données des seules ressources disponibles (fonction empty)
				if (empty($donnee['date_emprunt'])){
					echo '<tr>' . '<td>' . htmlspecialchars($donnee['id_document']) . '</td>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '</tr>';
				}
			}
			echo '</table>';
?>

<div>
	<form id="formPret3" action="../../controleur/mediatheque/pretC.php" method="post">
		<fieldset>
			<legend>Prêt</legend>
			<div>
				<label for="identifiant">Identifiant (id) de la ressource à prêter : </label>
				<select name="identifiant" id="identifiantRD" autofocus>
					<?php foreach ($donnees as $donnee){
						if (empty($donnee['date_emprunt'])){
					?>
					<option value="<?php echo htmlspecialchars($donnee['id_document']); ?>"><?php echo htmlspecialchars($donnee['id_document']); ?></option>
					<?php
						}
					}
					?>
				</select>
			</div>
			<div>
<!--champ "prenom" en autocomplétion (fonctionnalité <datalist>)-->
				<label for="prenom">Prénom de l'emprunteur : </label>
				<input type="text" name="prenom" id="prenom" list="completionP" required/>
				<datalist name="prenom" id="completionP">
					<?php
						include_once ('../../controleur/mediatheque/autocompletionC.php');
						foreach ($donneesP as $donneeP){
					?>
							<option value="<?php echo htmlspecialchars($donneeP['prenom']); ?>">
								<?php echo htmlspecialchars($donneeP['prenom']); ?>
							</option>
					<?php
						}
					?>
				</datalist>
			</div>
			<div>
<!--champ "nom" en autocomplétion (fonctionnalité <datalist>)-->
				<label for="nom">Nom de l'emprunteur : </label>
				<input type="text" name="nom" id="nom" list="completionN" required/>
				<datalist id="completionN">
					<?php
						foreach ($donneesN as $donneeN){
					?>
							<option value="<?php echo htmlspecialchars($donneeN['nom']); ?>">
								<?php echo htmlspecialchars($donneeN['nom']); ?>
							</option>
					<?php
						}
					?>
				</datalist>
			</div>
			<div>
<!--champ "email" en autocomplétion (fonctionnalité <datalist>)-->
				<label for="email">Email de l'emprunteur : </label>
				<input type="email" name="email" id="email" list="completionE" required/>
				<datalist id="completionE">
					<?php
						foreach ($donneesE as $donneeE){
					?>
							<option value="<?php echo htmlspecialchars($donneeE['email']); ?>">
								<?php echo htmlspecialchars($donneeE['email']); ?>
							</option>
					<?php
						}
					?>
				</datalist>
			</div>
			<div>
				<input type="submit" class="boutonV" name="pret" value="valider" />
			</div>
		</fieldset>
	</form>
</div>
	
<?php
		}else{
			echo '<p>Il n\'existe actuellement aucune ressource disponible répondant à la recherche effectuée.</p>';
		}
//RECUPERATION D'UNE RESSOURCE
	}elseif((isset($_POST['recuperer'])) AND (isset($donnees[0]))){
//récupération des valeurs de la colonne "date_emprunt" (fonction array_column)
		$emprunt = array_column($donnees, 'date_emprunt');
/*recherche de l'existence d'une valeur non NULL dans le tableau contenant les valeurs de la colonne "date_emprunt" (fonction in_array)
Dans l'affirmative, affichage des ressources prêtées répondant à la requête */
		if (in_array(!NULL, $emprunt)){
			echo'<table>
			<tr>
			<th>id</th>
			<th>type</th>
			<th>titre</th>
			<th>auteur</th>
			<th>édition</th>
			<th>langue</th>
			<th>commentaire</th>
			<th>prêté à</th>
			</tr>';
			
			foreach($donnees as $donnee){
//affichage des données des seules ressources prêtées (fonction empty)
				if (!empty($donnee['date_emprunt'])){
					echo '<tr>' . '<td>' . htmlspecialchars($donnee['id_document']) . '</td>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '<td>' . htmlspecialchars($donnee['prenom']) . ' ' . htmlspecialchars($donnee['nom']) . '</td>' . '</tr>';
				}
			}
		echo '</table>';
?>

<div>
	<form class="formModification2" action="../../controleur/mediatheque/recupC.php" method="post">
		<fieldset>
			<legend>Retour</legend>
			<div>
				<label for="identifiant">Identifiant (id) de la ressource à récupérer : </label>
				
				<select name="identifiant" id="identifiantRP" autofocus>
					<?php foreach ($donnees as $donnee){
						if (!empty($donnee['date_emprunt'])){
					?>
					<option value="<?php echo htmlspecialchars($donnee['id_document']); ?>"><?php echo htmlspecialchars($donnee['id_document']); ?></option>
					<?php
						}
					}
					?>
				</select>
			</div>
			<div>
				<input type="submit" class="boutonV" name="recup" value="valider" />
			</div>
		</fieldset>
	</form>
</div>
	
<?php
		}else{
			echo '<p>Il n\'existe actuellement aucune ressource prêtée répondant à la recherche effectuée.</p>';
		}

	}elseif(((isset($_POST['preter'])) OR (isset($_POST['recuperer']))) AND !(isset($donnees[0]))){
		echo '<p>Il n\'existe aucune ressource répondant à la recherche effectuée.</p>';
	}
?>

<?php
	if (isset($_POST['pret'])){
		if ($verifEmail !== false){
			echo '<p>Le ' . htmlspecialchars($donneesA[0]['type']) . ' intitulé "<strong>' . htmlspecialchars($donneesA[0]['titre']) . '</strong>" de ' . htmlspecialchars($donneesA[0]['auteur']) . ' a été prété à ' . htmlspecialchars($_POST['prenom']) . ' ' . htmlspecialchars($_POST['nom']) . '.</p>';
		}else{
			echo '<p>Le prêt n\'a pu être effectué car l\'adresse email saisie "<strong>' . htmlspecialchars($_POST['email']) . '</strong>" n\'est pas valide !</p>';
			echo '<p>Veuillez recommencer...</p>';
		}
	}elseif (isset($_POST['recup'])){
		echo '<p>Le ' . htmlspecialchars($donneesA[0]['type']) . ' intitulé "<strong>' . htmlspecialchars($donneesA[0]['titre']) . '</strong>" de ' . htmlspecialchars($donneesA[0]['auteur']) . ' est de nouveau disponible.</p>';
	}
	
	include_once ('piedpage.php');
?>