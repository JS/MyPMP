<?php
	include_once ('entete.php');

//vérif si identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');
?>

<h2>Suppression d'une ressource :</h2>
<h3>Quelle ressource souhaitez-vous supprimer ?</h3>
<div>
	<form class="formModification1" action="../../controleur/mediatheque/rechercheC.php" method="post">
		<fieldset>
			<legend>Recherche</legend>
			<div>
				<label for="type">Type de ressource : </label>
				<select name="type" id="type">
					<option value="tous">tous</option>
					<option value="livre">livre</option>
					<option value="magazine">magazine</option>
					<option value="CD audio">CD audio</option>
					<option value="DVD">DVD</option>
					<option value="autre">autre</option>
				</select>
			</div>
			<div>
				<label for="recherche">Rechercher : </label>
				<input type="text" name="recherche" id="recherche" required/>
			</div>
			<div>
				<input type="submit" class="boutonV" name="supprimer" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<table>
	<?php
//récupération des données correspondant à la recherche effectuée et présentation tabulaire
		if (isset($_POST['supprimer'])){
				echo'<tr>
				<th>id</th>
				<th>type</th>
				<th>titre</th>
				<th>auteur</th>
				<th>édition</th>
				<th>langue</th>
				<th>commentaire</th>
				</tr>';
	
			foreach($donnees as $donnee){
				echo '<tr>' . '<td>' . htmlspecialchars($donnee['id_document']) . '</td>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '</tr>';
			}
		}
	?>
</table>
<?php
	if (isset($donnee['id_document'])){
?>
<div>
	<form class="formModification2" action="../../controleur/mediatheque/suppressionC.php" method="post">
		<fieldset>
			<legend>Suppression</legend>
			<div>
				<label for="identifiantRS">Identifiant (id) de la ressource à supprimer : </label>
				
<!--				
				<input type="text" name="identifiant" id="identifiant" required/>
-->
				<select name="identifiant" id="identifiantRS" autofocus>
<!--récupération des id correspondant aux ressources proposées (menu déroulant)-->
					<?php foreach ($donnees as $donnee){
						if (!empty($donnee['id_document'])){
					?>
					<option value="<?php echo htmlspecialchars($donnee['id_document']); ?>"><?php echo htmlspecialchars($donnee['id_document']); ?></option>
					<?php
						}
					}
					?>
				</select>

			</div>
			<div>
				<input type="submit" class="boutonV" name="suppression" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
	}
	if (isset($_POST['suppression'])){
		foreach($donneesA as $donneeA){
			echo 'le '. htmlspecialchars($donneeA['type']) . ' ' . htmlspecialchars($donneeA['titre']) . ' a été définitivement supprimé !';
		}
	}
	include_once ('piedpage.php');
?>