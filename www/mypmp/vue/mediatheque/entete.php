<?php
	session_start();
?>

<!DOCTYPE html>

<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>mediatheque</title>
		<link rel="stylesheet" type="text/css" href="/mypmp/vue/mediatheque/style.css" />
	</head>

	<body>
		<header>
			<div>
				<h1>MyPMP</h1>
			<h2>Ma P'tite Médiathèque Partagée</h2>
			</div>
		</header>
		
<?php
	if(isset($_SESSION['profil'])){
		if($_SESSION['profil']==1){
?>
<!--barre de navigation "administrateur"-->
		<nav>
			<ul>
				<li><a href="/mypmp/mediatheque.php">Accueil</a></li>
				<li><a href="/mypmp/vue/mediatheque/consultation.php">Consultation</a></li>
				<li><a href="/mypmp/vue/mediatheque/recherche.php">Recherche</a></li>
				<li><a href="/mypmp/vue/mediatheque/admin.php">Administration</a></li>
				<li><a href="/mypmp/controleur/mediatheque/deconnexionC.php">Déconnexion</a></li>
			</ul>
		</nav>
<?php
		}
	}else{
?>
<!--barre de navigation "visiteur"-->
		<nav>
			<ul>
				<li><a href="/mypmp/mediatheque.php">Accueil</a></li>
				<li><a href="/mypmp/vue/mediatheque/consultation.php">Consultation</a></li>
				<li><a href="/mypmp/vue/mediatheque/recherche.php">Recherche</a></li>
				<li><a href="/mypmp/vue/mediatheque/connexion.php">Connexion</a></li>
			</ul>
		</nav>
<?php
	}
?>
		<section>