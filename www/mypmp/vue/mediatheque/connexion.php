<?php
	include_once ('entete.php');

// si une tentative de connexion ne peut aboutir (mauvais identifiants) :
	if (isset($resultat)){
		if (!$resultat){
			echo '<p>' . 'Mauvais identifiant ou mot de passe !' . '</p>';
			echo '<p>' . 'Veuillez recommencer...' . '</p>';
		}
	}
?>

<h2>Connexion</h2>
<div>
	<form id="formConnexion" action="../../controleur/mediatheque/connexionC.php" method="post">
		<fieldset>
			<legend>Ouvrir une session</legend>
			<div>
				<label for="login">identifiant :</label>
				<input type="text" name="login" placeholder="login" id="login" required autofocus />
			</div>
			<div>
				<label for="mdp">mot de passe :</label>
				<input type="password" name="mdp" placeholder="password" id="mdp" required />
			</div>
			<div>
				<input class="boutonV" type="submit" name="connexion" value="se connecter" />
			</div>
		</fieldset>
	</form>
</div>

<?php
	include_once ('piedpage.php');
?>