<?php
	include_once ('entete.php');
	
//vérif si identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');
?>

<h2>Modification d'une ressource</h2>
<h3>Quelle ressource souhaitez-vous modifier ?</h3>
<div>
	<form class="formModification1" action="../../controleur/mediatheque/rechercheC.php" method="post">
		<fieldset>
			<legend>Recherche</legend>
			<div>
				<label for="type">Type de ressource : </label>
				<select name="type" id="type" autofocus>
					<option value="tous">tous</option>
					<option value="livre">livre</option>
					<option value="magazine">magazine</option>
					<option value="CD audio">CD audio</option>
					<option value="DVD">DVD</option>
					<option value="autre">autre</option>
				</select>
			</div>
			<div>
				<label for="recherche">Rechercher : </label>
				<input type="text" name="recherche" id="recherche" required />
			</div>
			<div>
				<input type="submit" class="boutonV" name="modifier" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
//récupération des données correspondant à la recherche effectuée et présentation tabulaire
	if (isset($_POST['modifier']) AND isset($donnees[0])){
		echo'<table>
			<tr>
			<th>id</th>
			<th>type</th>
			<th>titre</th>
			<th>auteur</th>
			<th>édition</th>
			<th>langue</th>
			<th>commentaire</th>
			</tr>';
	
		foreach($donnees as $donnee){
			echo '<tr>' . '<td>' . htmlspecialchars($donnee['id_document']) . '</td>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '</tr>';
		}
		echo'</table>';
?>

<div>
	<form class="formModification2" action="../../controleur/mediatheque/modificationC.php" method="post">
		<fieldset>
			<legend>Sélection</legend>
			<div>
				<label for="identifiantRM">Identifiant (id) de la ressource à modifier : </label>
				<select name="identifiant" id="identifiantRM" autofocus>
<!--récupération des id correspondant aux ressources proposées (menu déroulant)-->
					<?php foreach ($donnees as $donnee){
						if (!empty($donnee['id_document'])){
					?>
					<option value="<?php echo htmlspecialchars($donnee['id_document']); ?>"><?php echo htmlspecialchars($donnee['id_document']); ?></option>
					<?php
						}
					}
					?>
				</select>			
			</div>
			<div>
				<input type="submit" class="boutonV" name="modification" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
	}elseif (isset($_POST['modifier']) AND !isset($donnees[0])){
		echo '<p>Il n\'existe aucune ressource répondant à la recherche effectuée.</p>';

	}elseif (isset($_POST['modification'])){
//affichage des données de la ressource à modifier
		foreach($donneesA as $donneeA){
			echo '<div><form class="formAjout" action="../../controleur/mediatheque/modificationC.php" method="post"><fieldset>
			<legend>Modification</legend><div>
			<label for="id">Identifiant (id) : </label>
			<input type="text" name="identifiant" id="id" value="' . htmlspecialchars($donneeA['id_document']) . '" readonly required /></div><div>
			<label for="type">Type de ressource : </label>
			<input type="text" name="type" id="type" value="' . htmlspecialchars($donneeA['type']) . '" readonly required /></div><div>
			<label for="titre">Titre : </label>
			<input type="text" name="titre" id="titre" value="' . htmlspecialchars($donneeA['titre']) . '" required /></div><div>
			<label for="auteur">Auteur / interprète : </label>
			<input type="text" name="auteur" value="' . htmlspecialchars($donneeA['auteur']) . '" id="auteur" /></div><div>	
			<label for="annee">Année d\'édition : </label>
			<input type="text" name="annee" value="' . htmlspecialchars($donneeA['annee_edition']) . '" id="annee" /></div><div>
			<label for="langue">Langue : </label>
			<input type="text" name="langue" value="' . htmlspecialchars($donneeA['langue']) . '" id="langue" /></div><div>
			<label for="commentaire">Commentaire : </label>
			<textarea name="commentaire" "id="commentaire">' . htmlspecialchars($donneeA['commentaire']) . '</textarea></div><div>
			<input type="submit" class="boutonV" name="modification" value="modifier" /></div>
			</fieldset></form></div>';
		}
	}
	
	include_once ('piedpage.php');
?>