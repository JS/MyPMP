<?php
	include_once ('entete.php');
?>

<h2>Consultation</h2>
<div>
<!--boutons permettant de choisir le type de ressources à afficher (formulaire)-->
	<form id="formConsultation" action="consultation.php" method="post">
		<input class="bouton" type="submit" name="choix" value="tout" />
		<input class="bouton" type="submit" name="choix" value="les livres" />
		<input class="bouton" type="submit" name="choix" value="les magazines" />
		<input class="bouton" type="submit" name="choix" value="les CD audio" />
		<input class="bouton" type="submit" name="choix" value="les DVD" />
		<input class="bouton" type="submit" name="choix" value="autres" />
	</form>
</div>
<table>
	<tr>
		<th>type</th>
		<th>titre</th>
		<th>auteur</th>
		<th>édition</th>
		<th>langue</th>
		<th>commentaire</th>
	</tr>
	
	<?php
		include_once ('../../controleur/mediatheque/consultationC.php');
//récupération et affichage des données
		foreach($donnees as $donnee){
			echo '<tr>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '</tr>';
		}
	?>

</table>

<?php
//affichage des numéros de page (lien vers la page souhaitée)
	echo '<p id="paginationConsultation"><span>pages : </span>';
	$pagination = 0;
	while ($pagination < $nbPages){
//utilisation de GET (via lien) pour affichage de la page souhaitée et transmission du choix du type de ressources demandé
		echo '<span><a href="?p=' . ($pagination + 1) . '&choix=' . ($_POST['choix']) . '"> ' . ($pagination + 1) . ' </a></span>';
		$pagination++;
	}
	echo '</p>';
?>

<?php
include_once ('piedpage.php');
?>