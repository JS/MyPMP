<?php
	include_once ('entete.php');
?>

<h1>Bienvenue dans Ma P'tite Médiathèque Partagée !</h1>

<?php
// affichage du nombre de ressources stockées dans la base de données
	echo '<p>A ce jour, Ma P\'tite Médiathèque comprend ' . $nbRessources[0] . ' ressources.</p>'; 
?>

<p>Dernières ressources saisies :</p>
<table>
	<tr>
		<th>type</th>
		<th>titre</th>
		<th>auteur</th>
		<th>édition</th>
		<th>langue</th>
		<th>commentaire</th>
	</tr>
	
<?php
//récupération et affichage des dernières ressources saisies
	foreach($ressources as $donnee){
		echo '<tr>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '</tr>';
	}
?>

</table>

<?php
	include_once ('piedpage.php');
?>