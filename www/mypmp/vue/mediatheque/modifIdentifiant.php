<?php
	include_once ('entete.php');

//vérif si identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');
?>

<h2>Changement de l'identifiant et du mot de passe</h2>
<div>
	<form id="formModifIdentifiant" action="../../controleur/mediatheque/identifiantC.php" method="post">
		<fieldset>
			<legend>Modification</legend>
			<div>
				<label for="login">nouvel identifiant :</label>
				<input type="text" name="login" placeholder="login" id="login" required autofocus />
			</div>
			<div>
				<label for="mdp">nouveau mot de passe :</label>
				<input type="password" name="mdp" placeholder="password" id="mdp" required />
			</div>
			<div>
				<input class="boutonV" type="submit" name="connexion" value="modifier" />
			</div>
		</fieldset>
	</form>
</div>

<?php
	include_once ('piedpage.php');
?>