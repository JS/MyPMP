<?php
	include_once ('entete.php');

//vérif si identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');
?>

<h2>Que souhaitez-vous faire <?php echo' '. htmlspecialchars($_SESSION['login']) . ' ';?> ?</h2>

<div>
<!--actions possibles (choix via formulaire)-->
	<form id="formAdministration" action="../../vue/mediatheque/admin.php" method="post">
		<fieldset>
			<legend>Administration</legend>
			<div>
				<input type="radio" name="choix" value="consulter" id="consulter" checked />
				<label for="consulter">consulter la liste des ressources</label>
			</div>
			<div>
				<input type="radio" name="choix" value="rechercher" id="rechercher" />
				<label for="rechercher">rechercher une ressource</label>
			</div>
			<div>
				<input type="radio" name="choix" value="ajouter" id="ajouter"/>
				<label for="ajouter">ajouter une ressource</label>
			</div>
			<div>
				<input type="radio" name="choix" value="modifier" id="modifier"/>
				<label for="modifier">modifier une ressource</label>
			</div>
			<div>
				<input type="radio" name="choix" value="supprimer" id="supprimer"/>
				<label for="supprimer">supprimer une ressource</label>
			</div>
			<div>
				<input type="radio" name="choix" value="preter" id="preter"/>
				<label for="preter">prêter / récupérer une ressource</label>
			</div>
			<div>
<!-- A FAIRE !!! -->
				<input type="radio" name="choix" value="consulterPrets" id="consulterPrets"/>
				<label for="preter">consulter les ressources prêtées</label>
			</div>
			<div>
				<input type="radio" name="choix" value="identifier" id="identifier"/>
				<label for="identifier">modifier ses identifiants</label>
			</div>
			<div>
				<input type="submit" class="boutonV" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
//redirection selon choix de l'action souhaitée
	if (isset($_POST['choix'])){
		if ($_POST['choix']=='consulter'){
			header ('Location: /mypmp/vue/mediatheque/consultation.php');
		}elseif ($_POST['choix']=='rechercher'){
			header ('Location: /mypmp/vue/mediatheque/recherche.php');
		}elseif ($_POST['choix']=='ajouter'){
			header ('Location: /mypmp/vue/mediatheque/ajout.php');
		}elseif ($_POST['choix']=='modifier'){
			header ('Location: /mypmp/vue/mediatheque/modification.php');
		}elseif ($_POST['choix']=='supprimer'){
			header ('Location: /mypmp/vue/mediatheque/suppression.php');
		}elseif ($_POST['choix']=='preter'){
			header ('Location: /mypmp/vue/mediatheque/pret.php');
		}elseif ($_POST['choix']=='consulterPrets'){
// A FAIRE !!!
			header ('Location: /mypmp/vue/mediatheque/pret.php');
		}elseif ($_POST['choix']=='identifier'){
			header ('Location: /mypmp/vue/mediatheque/modifIdentifiant.php');
		}
	}
	include_once ('piedpage.php');
?>