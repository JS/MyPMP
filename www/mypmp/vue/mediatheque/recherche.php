<?php
	include_once ('entete.php');

// si une recherche lancée est infructueuse (ressource inconnue) :
	if (isset($donnees)){
		if (!$donnees){
			echo '<p>' . 'Il n\'existe aucune ressource répondant à la recherche effectuée.' . '</p>';
			echo '<p>' . 'Veuillez recommencer...' . '</p>';
		}
	}		
?>

<h2>Recherche</h2>
<div>
<!--choix du type de recherche (simple ou avancé)-->
	<form class="formRecherche" action="../../vue/mediatheque/recherche.php" method="post">
		<input class="bouton" type="submit" name="choix" value="recherche" />
		<input class="bouton" type="submit" name="choix" value="recherche avancée" />
	</form>
</div>

<?php
//affichage de la zone de recherche simple
	if (!(isset($_POST['choix'])) OR ($_POST['choix'] == 'recherche')){
?>

<div>
	<form class="formRecherche" action="../../controleur/mediatheque/rechercheC.php" method="post">
		<fieldset>
			<legend>Recherche</legend>
			<div>
				<label for="recherche">Rechercher : </label>
				<input type="search" name="recherche" id="recherche" autocomplete="on" required autofocus/>
			</div>
			<div>
				<input type="submit" class="boutonV" name="rechercher" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
//affichage de la zone de recherche avancée
//A FAIRE !
	}elseif ($_POST['choix'] == 'recherche avancée'){
?>

<div>
	<form class="formRecherche" action="../../controleur/mediatheque/rechercheC.php" method="post">
		<fieldset>
			<legend>Recherche avancée</legend>
			<div>
				<label for="type">Rechercher : </label>
				<select name="type" id="type">
					<option value="0">une ressource...</option>
					<option value="1">un auteur...</option>
					<option value="2">un utilisateur...</option>
				</select>
			</div>
			<div>
				<label for="recherche">Contenant : </label>
				<input type="search" name="recherche" id="recherche" autocomplete="on" required autofocus/>
			</div>
			<div>
				<input type="submit" class="boutonV" name="rechercher" value="valider" />
			</div>
		</fieldset>
	</form>
</div>

<?php
	}
?>

<table>
	<?php
		if (isset($_POST['rechercher']) AND ($donnees)){
				echo'<tr>
				<th>type</th>
				<th>titre</th>
				<th>auteur</th>
				<th>édition</th>
				<th>langue</th>
				<th>commentaire</th>
				</tr>';
//récupération et affichage des données correspondant à la recherche
			foreach($donnees as $donnee){
				echo '<tr>' . '<td>' . htmlspecialchars($donnee['type']) . '</td>' . '<td>'. htmlspecialchars($donnee['titre']) . '</td>' . '<td>' . htmlspecialchars($donnee['auteur']) . '</td>' . '<td>'. htmlspecialchars($donnee['annee_edition']) . '</td>' . '<td>' . htmlspecialchars($donnee['langue']) . '</td>' . '<td>' . htmlspecialchars($donnee['commentaire']) . '</td>' . '</tr>';
			}
		}
	?>
</table>

<?php
	include_once ('piedpage.php');
?>