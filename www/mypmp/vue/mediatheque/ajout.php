<?php
	include_once ('entete.php');

//vérif si identification préalable de l'utilisateur
	include_once ('../../controleur/mediatheque/authentificationC.php');

	if (isset($_POST['ajouter'])){
//vérif des zones "type" et "titre" (doivent être renseignées) 
		if (empty($_POST['type']) OR empty($_POST['titre'])){
			echo '<p>' . 'Le type et le titre de la ressource doivent obligatoirement être renseignés !' . '</p>';
			echo '<p>' . 'Veuillez recommencer...' . '</p>';
		}else{
//confirmation de l'ajout
			echo '<p>' . 'Le document intitulé "<strong>' . htmlspecialchars($_POST['titre']) . '</strong>" a été ajouté à la liste des ressources disponibles.' . '</p>';
		}
	}
?>

<h2>Ajout d'une nouvelle ressource</h2>
<div>
<!--saisie des informations relatives à la nouvelle ressource (via formulaire)-->
	<form class="formAjout" action="../../controleur/mediatheque/ajoutC.php" method="post">
	<fieldset>
		<legend>Ajout</legend>
		<div>
			<label for="type">Type de ressource : </label>
			<select name="type" id="type" autofocus>
				<option value="1">livre</option>
				<option value="2">magazine</option>
				<option value="3">CD audio</option>
				<option value="4">DVD</option>
				<option value="5">autre</option>
			</select>
		</div>
		<div>
			<label for="titre">Titre : </label>
			<input type="text" name="titre" id="titre" required />
		</div>
		<div>
<!--champ "auteur" en autocomplétion (fonctionnalité <datalist>)-->
			<label for="auteur">Auteur / interprète : </label>
			<input type="text" name="auteur" id="auteur" placeholder="Prénom1 Nom1, Prénom2 Nom2..." list="completionA" />
			<datalist id="completionA">
				<?php
					include_once ('../../controleur/mediatheque/autocompletionC.php');
					foreach ($donneesA as $donneeA){
				?>
				<option value="<?php echo htmlspecialchars($donneeA['auteur']); ?>">
					<?php echo htmlspecialchars($donneeA['auteur']); ?> 
				</option>
				<?php
					}
				?>
			</datalist>
		</div>
		<div>
			<label for="annee">Année d'édition : </label>
			<input type="text" name="annee" id="annee" placeholder="1901" pattern="(19|20)([0-9]{2})" />
		</div>
		<div>
<!--champ "langue" en autocomplétion (fonctionnalité <datalist>)-->
			<label for="langue">Langue : </label>
			<input type="text" name="langue" id="langue" list="completionL" />
			<datalist id="completionL">
				<?php
					foreach ($donneesL as $donneeL){
				?>
				<option value="<?php echo htmlspecialchars($donneeL['langue']); ?>">
					<?php echo htmlspecialchars($donneeL['langue']); ?> 
				</option>
				<?php
					}
				?>
			</datalist>
		</div>
		<div>
			<label for="commentaire">Commentaire : </label>
			<textarea name="commentaire" id="commentaire"></textarea>
		</div>
		<div>
			<input type="reset" class="boutonV" value="annuler" />
		</div>
		<div>
			<input type="submit" class="boutonV" name="ajouter" value="valider" />
		</div>
	</fieldset>
	</form>
</div>
		
<?php
	include_once ('piedpage.php');
?>