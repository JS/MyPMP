<div>
	<form id="formPret2" action="../../controleur/mediatheque/rechercheC.php" method="post">
		<fieldset>
			<legend>Recherche</legend>
			<div>
				<label for="type">Type de ressource : </label>
				<select name="type" id="type">
					<option value="tous">tous</option>
					<option value="livre">livre</option>
					<option value="magazine">magazine</option>
					<option value="CD audio">CD audio</option>
					<option value="DVD">DVD</option>
					<option value="autre">autre</option>
				</select>
			</div>
			<div>
				<label for="recherche">Rechercher : </label>
				<input type="text" name="recherche" id="recherche" required autofocus/>
			</div>
			<div>