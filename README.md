# MyPMP
*Ma Petite Médiathèque Partagée*

## Le projet initial :
Ce projet de stage s'inscrit dans le cadre de la formation suivie auprès du Cnam (Conservatoire national des arts et métiers) et intitulée "*Technicien maintenance micro., réseaux et internet*".  
Il consiste en la réalisation d'une application web de gestion de médiathèque personnelle qui doit pouvoir proposer les fonctionnalités suivantes :

* consultation,
* recherche,
* ajout,
* modification,
* suppression,
* prêt de ressources variées (livres, magazines, CD audio, DVD...).

Les technologies utilisées pour sa conception sont : Linux, Apache, HTML, CSS, PHP et MySQL.

## L'évolution du projet :
#### A l'issue du stage (31/01/2017) :
L'application est globalement fonctionnelle. Du point de vue de l'utilisateur, elle se compose de 2 interfaces. La première permet consultation et recherche parmi les ressources disponibles. La seconde, à l'accès restreint et protégé, est destinée à la gestion des documents (ajout, modification...).  
Si les principales fonctionnalités sont en place, l'application reste cependant non finalisée et très insuffisamment testée.

##### Quelques captures d'écran :

![accueil](images/captures/c01s_accueil.png)  
![consultation](images/captures/c02s_consultation.png)  
![recherche](images/captures/c03s_recherche.png)  
![connexion](images/captures/c04s_connexion.png)  
![administration](images/captures/c05s_administration.png)  
![ajout](images/captures/c06s_ajout.png)